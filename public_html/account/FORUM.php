<!DOCTYPE HTML>
<html>

<head>
    <title>Întrebări</title>


 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <script src="java/jquery.min.js"></script>
    <script src="java/skel.min.js"></script>
    <script src="java/skel-layers.min.js"></script>
    <script src="java/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/status%20bar.css" />
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="/css/style-xlarge.css" />
             <link rel="stylesheet" href="css/search.css" />
		</noscript>
<style>

.search-container{
  width: 490px;
  display: block;
  margin: 0 auto;
}

  .button-intrebare {
     cursor: pointer;background-color: #008CBA;
    border: none;
    color: white;
    
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    }
    }
    
input#search-bar{
  margin: 0 auto;
  width: 100%;
  height: 45px;
  padding: 0 20px;
  font-size: 1rem;
  border: 1px solid #D0CFCE;
  outline: none;
  &:focus{
    border: 1px solid #008ABF;
    transition: 0.35s ease;
    color: #008ABF;
    &::-webkit-input-placeholder{
      transition: opacity 0.45s ease; 
  	  opacity: 0;
     }
    &::-moz-placeholder {
      transition: opacity 0.45s ease; 
  	  opacity: 0;
     }
    &:-ms-placeholder {
     transition: opacity 0.45s ease; 
  	 opacity: 0;
     }    
   }
 }

form.bar input[type=text] {
    padding: 10px;
    font-size: 17px;
    border: 1px solid grey;
    float: left;
    width: 80%;
    background: #f1f1f1;
}

	body {
		
        	background-image: url("imagini/background.jpg") ;
		background-repeat: repeat;
		
	}

  table {
  border-width: 3px;
  background-color: black;
}
tr, td {
  padding: 2px;
}

.box {

    background-color: #f1f1f1;
    padding: 0.01em 16px;
    margin: 20px 0;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;

}

</style>
   
</head>

<body  >

    <header id="header" class="skel-layers-fixed">
        <h1 style="font-size:16px"  ><a href="FORUM.php">Întrebări</a></h1>
        <nav id="nav">
            <ul>
                 
                  <li><a href="main.php" class="button special">ACASĂ</a></li>
                <li> <a href="TIPURI DE ALERGII.html" >TIPURI DE ALERGII</a> </li>
                <li> <a href="ASISTENT.html" >ASISTENT</a> </li>
                <li><a href="harta%20judete.html" >LOCAȚIE</a></li>
                <li><a href="FORUM.php">ÎNTREBĂRI</a></li>
                <li><a href="about.html">DESPRE PAGINĂ</a></li>
                <li><a href="logout.php" style="color: red;">Deconectare</a></li>        
            </ul>
        </nav>
    </header>
    
    
<br>

       <section>
        <div class="container">
                  <?php
                     include 'header.php';
                  ?>
                         <h2 align="center">Introduceți întrebarea dumneavoastră</h2>
            <form class="bar" action="search.php" method="POST">
<input type="text" name="search"  placeholder="cauta intrebarea" required
    size="30" minlength="4" >
<button type="submit" name="submit-search"  class="button special" style="height: 2.75em;
    line-height: 2.75em;
    margin-bottom: 0;
    padding: 0 1em;
    position: relative;
   
    vertical-align: middle;" >Caută</button> 
</form>
      <span class="validity"></span>
  </div>
</section>  

<section id="three" style="margin-top: 50px;">
        <div class="container">
            <div class="row">
                <div class="8u">
                    <section>
                       <?php
include 'header.php';

    $sql = "SELECT * FROM article";
    $result = mysqli_query($conn, $sql);
    $queryResults = mysqli_num_rows($result);
    if($queryResults > 0)
    {
      while($row = mysqli_fetch_assoc($result)){
        echo"<a href='article.php?title=".$row['a_title']."&date=".$row['a_dat']."&hmm=".$row['a_id']."'>
         <div class='box'> 
            <p>".$row['a_title']."</p>
            <p>".$row['a_author']."</p>
            </div></a><br>";
      }
    }
 ?>
 <h1 align='center'>Puteți adăuga o întrebare apăsând pe butonul de mai jos</h1>
                 <div align='center'><a href='intrebare/index.php' ><button class='button-intrebare' >Adaugă</button></a></div> 
                 <br><br>
                    </section>
                </div>

                <div class="4u">
                    <section>
                       <!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9692215;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>

  
<!-- End of LiveChat code -->
                    </section>
                    
                  
                </div>
            </div>
        </div>
    </section>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </body>
</html>
